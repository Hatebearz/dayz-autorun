#SingleInstance force

SprintTime := 15000
IsRunning := False

AnglePoints := 20
AngleTime := 1000
AngleOnRight := True
IsAngling := False

StartRunning() {
    Send {vk57 Down}
}

StopRunning() {
    Send {vk57 Up}
}

StartSprinting() {
    global SprintTime
    SetTimer Sprint, % 2 * SprintTime
    Sprint()
}

StopSprinting() {
    Send {ShiftUp}
    SetTimer Sprint, Delete
}

Sprint() {
    global SprintTime
    global IsRunning
    Send {ShiftDown}
    numberOfGranularSleeps := % SprintTime / 100
    Loop, % numberOfGranularSleeps {
        Sleep 100
        if (not IsRunning) {
            return
        }
    }
    Send {ShiftUp}
    return
}

StartAngling() {
    global AnglePoints
    global AngleTime
    global AngleOnRight
    
    MouseMove, % AnglePoints, 0, 100, R
    AngleOnRight := True  
    SetTimer ChangeAngle, % 2 * AngleTime
}

StopAngling() {
    global AnglePoints
    global AngleOnRight
    global IsAngling

    SetTimer ChangeAngle, Delete

    if (AngleOnRight) {
        MouseMove, % -AnglePoints, 0, 100, R
    }
    else {
        MouseMove, % AnglePoints, 0, 100, R
    }

}

ChangeAngle() {
    global AnglePoints
    global AngleOnRight

    if (AngleOnRight) {
        MouseMove % -2 * AnglePoints, 0, 100, R
        AngleOnRight := False
    }
    else {
        MouseMove % 2 * AnglePoints, 0, 100, R
        AngleOnRight := True
    }
}

Home::
+Home::
    if (IsRunning) {
        IsRunning := False
        StopRunning()
        StopSprinting()
    }
    else {
        IsRunning := True
        StartRunning()
        StartSprinting()
    }
return

; End::
;     if (IsAngling) {
;         StopAngling()
;         IsAngling := False
;     }
;     else {
;         StartAngling()
;         IsAngling := True
;     }
; return