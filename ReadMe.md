# DayZ AutoRun

Скрипт AutoHotKey, позволяющий включать и отключать бег с периодическим (раз в 15 секунд) спринтом.

## How-to
1. Скачать текущий репозиторий;
2. Разархировать DayZAutoRun.exe;
3. Запустить DayZAutoRun.exe;
4. Включать/выключать режим бега, нажимая на кнопку Home.